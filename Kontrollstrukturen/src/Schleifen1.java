import java.util.Scanner;
public class Schleifen1 {			

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		
		System.out.print("Geben Sie eine Zahl ein: ");
		int zahl = tastatur.nextInt();
		
		// Aufw�rts z�hlend
		for (int i = 1; i <= zahl; i++) {
			
			System.out.println(i);
		}
		
			System.out.println();
		
		// Abw�rts z�hlend
		int j = zahl;
		while (j >= 1) {
			
			System.out.println(j);
			j--;
		}

	}

}
