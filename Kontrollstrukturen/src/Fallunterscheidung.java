import java.util.Scanner;
public class Fallunterscheidung {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		
		System.out.print("Geben Sie eine Note ein: ");
		byte note = tastatur.nextByte();
		
		switch (note)
		{ 
		case 1:
			System.out.println("sehr gut");
			break;
		case 2:
			System.out.println("gut");
			break;
		case 3:
			System.out.println("befriedigend");
			break;
		case 4:
			System.out.println("ausreichend");
			break;
		case 5:
			System.out.println("mangelhaft");
			break;
		case 6:
			System.out.println("ungenügend");
			break;
		default:
			System.out.println("Fehler!");
			break;
			
		
		}
		
		
	}
}