import java.util.Scanner;
public class Schleifen1A2 {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		
		System.out.print("Geben Sie bitte einen begrenzenden Wert ein: ");
		int wert = tastatur.nextInt();
		
		// Teil A
		int summeA = 0;
		for (int i = 1; i <= wert; i++) {
			
			summeA += i;
			
		}
		System.out.println("Die Summe f�r a) betr�gt: " + summeA);
		
		// Teil B
		int summeB = 0;
		for (int i = 2; i <= wert; i += 2) {
			
			summeB += i;
		}
		System.out.println("Die Summe f�r b) betr�gt: " + summeB);
		
		// Teil C
		int summeC = 0;
		for (int i = 1; i <= wert; i += 2) {
					
			summeC += i;
		}
		System.out.println("Die Summe f�r c) betr�gt: " + summeC);
	}

}
