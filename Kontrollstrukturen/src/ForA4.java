
public class ForA4 {

	public static void main(String[] args) {
		
		System.out.print("A: ");
		
		for (int i = 99; i >= 9; i -= 3) {
			System.out.print(i + " ");
		}
		
		System.out.println();
		System.out.print("E: ");
		
		int count = 99;
		
		do {
			
			System.out.print(count + " ");
			count -= 3;
			
		}
		while (count >= 9); 
			
			
			
		}
	}


