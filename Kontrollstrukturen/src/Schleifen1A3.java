
public class Schleifen1A3 {

	public static void main(String[] args) {
		int zahl = 1;
		
		while (zahl <= 200) {
			if ((zahl % 7 == 0) && (zahl % 5 > 0 && zahl % 4 == 0)) {
				System.out.println(zahl);
			}
			
			zahl ++;
		}

	}

}
