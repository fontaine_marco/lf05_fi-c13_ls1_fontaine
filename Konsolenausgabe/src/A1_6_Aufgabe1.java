
public class A1_6_Aufgabe1 {
	
	public static void main(String[] args) {
		
		String s = "**";
		System.out.printf( "%6s\n", s);
		
		String t = "*\t*";
		System.out.printf("%4s\n", t);
		
		String v = "*\t*";
		System.out.printf("%4s\n", v);
		
		String w = "**";
		System.out.printf( "%6s\n", w);
	}
}
