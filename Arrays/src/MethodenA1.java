
public class MethodenA1 {

	public static void main(String[] args) {
		
		int [] zahlenfeld = { 1, 2, 3, 7, 8};
		int [] schnubbi = {4, 5, 6, 7, 9};
		
		System.out.println(convertArrayToString (zahlenfeld));
		System.out.println(convertArrayToString (schnubbi));

	}
	
	public static String convertArrayToString(int[] zahlen) {
		
		String ergebnis = "[ ";
		for(int i=0; i < zahlen.length; i++) {
			ergebnis += zahlen[i] + " ";    
		}
		ergebnis += "]";
		return ergebnis;

		
		
	}
}
