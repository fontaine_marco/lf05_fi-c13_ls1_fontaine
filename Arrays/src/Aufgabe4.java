
public class Aufgabe4 {

	public static void main(String[] args) {
		
		int [] lotto = { 3, 7, 12, 18, 37, 42};
		
		for (int i = 0; i < lotto.length; i++) {
			
			if (i == 0)  {
				System.out.print("[ " + lotto[i] + " ");
			}
			
			else if (i == lotto.length - 1) {
				System.out.print(lotto[i] + " ]");
			}
			
			else {
				System.out.print(lotto[i] + " ");
			}
		}
		
		System.out.println();
		
		int kommt13vor = 0;
		
		for (int i = 0; i < lotto.length; i++) {
			
			if (lotto[i] == 12) {
				
				System.out.println("Die Zahl 12 ist in der Ziehung enthalten.");
				
			}
			
			if (lotto[i] !=13) {

				kommt13vor += 1;
				
			}
						
		}
		
		if (kommt13vor == lotto.length) {
			
			System.out.println("Die Zahl 13 ist nicht in der Ziehung enthalten.");
		}
		
	}

}
