

public class MethodenA4 {

	public static void main(String[] args) {

       double[][] ttable = createTempratureTable(8);
       printTempratureTable(ttable);
	}
	
	public static double[][] createTempratureTable(int rows) {
		double[][] temperaturtab = new double[rows][2];
		
		for (int i = 0; i < temperaturtab.length; i++) {
			temperaturtab[i][0] = (i+1) * 10.0;
			temperaturtab[i][1] = (5.0/9.0) * (temperaturtab[i][0] - 32);
		}
		
		return temperaturtab;
	}
	
	public static void printTempratureTable(double[][] temperaturtab) {	
		System.out.println(   "    Farenheit   |     Celsius    ");
		System.out.println(   "-------------------------------");
		for (int i = 0; i < temperaturtab.length; i++) {
			System.out.format("      %4.1f           %4.1f    %n",temperaturtab[i][0], temperaturtab[i][1]);
		}
	}

}
