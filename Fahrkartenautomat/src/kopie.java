import java.util.Scanner;

class kopie {
	public static double fahrkahrtenBestellungErfassen() {
		double [] preise = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
		String [] bezeichnungen = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", 
        		"Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC",
        		"Kleingruppen-Tageskarte Berlin ABC"};
		int auswahlnummer = 1;
		
		//5.3, 1) Der Vorteil durch die Verwendung von Arrays liegt darin, dass man neue Ticket- und Preis�nderung ganz einfach durchf�hren kann, indem man die Eintr�ge im
		// jeweiligen Array �ndert / hinzuf�gt. Der Methodencode bleibt unber�hrt und funktioniert direkt wie gehabt.
      	
		Scanner tastatur = new Scanner(System.in);
      	double zuZahlenderBetrag = 0;
      	int tarifwahl;
      	
      	do {
      		
      	System.out.println("W�hlen Sie bitte Ihren Wunschtarif aus: ");	
      	System.out.println();
      	System.out.printf("%-25s", "Auswahlnummer");
      	System.out.printf("%-60s", "Bezeichnung");
      	System.out.println("Preis");
      	
      	for (int i = 0; i < preise.length; i++) {
      		System.out.printf("%-25d", auswahlnummer);
      		System.out.printf("%-60s", bezeichnungen[i]);
      		System.out.printf("%.2f", preise[i]);
      		System.out.print("�");
      		System.out.println();
      		auswahlnummer ++;
      	}
      
      	// 5.3, 3) Ein eindeutiger Vorteil in dieser Implementierung mit Schleife und Array besteht darin, dass die Schleife auch bei �nderung des Arrays 
      	// weiterhin funktioniert und direkt die neuen Eintr�ge �bernimmt. Daher sind lediglich (bei �nderungswunsch) �nderung an den Array-Inhalten vorzunehmen.
      	// Die etwas "primitivere" Implementierung war daf�r einfacher zu programmieren und setzt keine Arrays o.�. voraus. Bei statischem Angebot also durchaus eine
      	// sinnvolle M�glichkeit. Bei l�ngeren, dynamischen Listen ist die Schleife mit Arrays deutlich eleganter und zeitsparender.
      	
      	tarifwahl = tastatur.nextInt();
      	if (tarifwahl < preise.length + 1) {
      		System.out.print("Ihre Wahl: " + bezeichnungen[tarifwahl - 1] + ". Preis: ");
      		System.out.printf("%.2f", preise[tarifwahl - 1]);
      		System.out.println("�");
      		zuZahlenderBetrag = preise [tarifwahl - 1];
      	}
      	else {
      		System.out.println(">>falsche Eingabe<<");
        	System.out.println("Bitte versuchen Sie es erneut.");
      		System.out.println();
      		auswahlnummer = 1;
      		
      	}
      	} while (tarifwahl > preise.length);
          
        byte ticketanzahl;
      	System.out.print("Anzahl der Tickets: ");
        ticketanzahl = tastatur.nextByte();      	
       
        zuZahlenderBetrag = zuZahlenderBetrag * ticketanzahl;
        
      	return zuZahlenderBetrag;
	}
	
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);
	    double eingezahlterGesamtbetrag;
	    double eingeworfeneM�nze;
	    
		   eingezahlterGesamtbetrag = 0.0;
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.printf("Noch zu zahlen: %.2f Euro\n" , (zuZahlenderBetrag - eingezahlterGesamtbetrag));
	    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");	    	   
	    	   eingeworfeneM�nze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneM�nze;
	           
	       }
	       return eingezahlterGesamtbetrag;
	}
	
	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
	       for (double i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
	}
	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		double r�ckgabebetrag;
		 r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       if(r�ckgabebetrag > 0.0)
	       {
	    	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO " , r�ckgabebetrag);
	    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

	           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
	           {
	        	  System.out.println("2 EURO");
		          r�ckgabebetrag -= 2.0;
	           }
	           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
	           {
	        	  System.out.println("1 EURO");
		          r�ckgabebetrag -= 1.0;
	           }
	           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
	           {
	        	  System.out.println("50 CENT");
		          r�ckgabebetrag -= 0.5;
	           }
	           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
	           {
	        	  System.out.println("20 CENT");
	 	          r�ckgabebetrag -= 0.2;
	           }
	           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
	           {
	        	  System.out.println("10 CENT");
		          r�ckgabebetrag -= 0.1;
	           }
	           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	 	          r�ckgabebetrag -= 0.05;
	           }
	       } 
	       
	       System.out.println("\nVergessen Sie nicht, den Fahrschein/die Fahrscheine\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir w�nschen Ihnen eine gute Fahrt.");
	}
	
    public static void main(String[] args)
    {
    	boolean weiter = true;   	
    	double zuZahlenderBetrag;
        double eingezahlterGesamtbetrag;     
        
        while (weiter == true) {
        	zuZahlenderBetrag = fahrkahrtenBestellungErfassen();
      	 	eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag); 	 
      	 	fahrkartenAusgeben();
            rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
            System.out.println();
            System.out.println("N�chster Kunde: ");
            System.out.println();
        }
                                   
      
        }
        
    }
    
    
    


// A2.5)
// 5.: Ich habe byte gew�hlt, da mit diesem Datentyp bis zu 127 Tickets gleichzeitig gekauft werden k�nnten,
// 	   was f�r einen Ticketautomaten absolut ausreichen sollte. 
// 6.: Die Variable �ticketanzahl� wird mit der Variablen �zuZahlenderBetrag� multipliziert, 
//	   um den Gesamtpreis auszurechnen. So wei� der Kunde direkt genau, was er an M�nzen in den Automaten einwerfen
// 	   muss.
//	   Der Nutzer gibt also erst den Einzelpreis (�zuZahlenderBetrag�) in die Konsole ein. 
// 	   Anschlie�end die Anzahl der Tickets (�ticketanzahl�). Die Konsole gibt folglich den Gesamtpreis aus 
// 	   (�Noch zu zahlen�).
	