import java.util.Scanner; // Import der Klasse Scanner 
	 
	public class KonsoleneingabeAufgabe2 {
	
	   
	  public static void main(String[] args) // Hier startet das Programm 
	  { 
	     
	    // Neues Scanner-Objekt myScanner wird erstellt     
	    Scanner myScanner = new Scanner(System.in);  
	     
	    System.out.print("Hallo, Nutzer! Wie hei�t du? ");    
	     
	    // Die Variable zahl1 speichert die erste Eingabe 
	    String name = myScanner.next();  
	     
	    System.out.print("Wie alt bist du? "); 
	     
	    // Die Variable zahl2 speichert die zweite Eingabe 
	    byte alter = myScanner.nextByte();  
	     
	        
	    System.out.print("\nName: "); 
	 	System.out.println(name);
	    
	    System.out.print("\nAlter: ");
	    System.out.println(alter);
	    
	   
	   
	    myScanner.close(); 
	     
	  }    
	} 


