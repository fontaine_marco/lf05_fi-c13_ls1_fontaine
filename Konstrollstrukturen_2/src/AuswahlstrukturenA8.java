import java.util.Scanner;
public class AuswahlstrukturenA8 {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		
		System.out.print("Geben Sie bitte eine Jahreszahl ein: ");
		int jahr = tastatur.nextInt();
		
		if (jahr%4 == 0 && jahr < 1582) {
			
		System.out.print("Das Jahr " + jahr + " ist ein Schaltjahr!");
			
					
		}
		else if (jahr >= 1582 && jahr%4 == 0 && (jahr%100 > 0 || jahr%400 == 0)) {
			System.out.print("Das Jahr " + jahr + " ist ein Schaltjahr!");
		}
		else {
			System.out.print("Das Jahr " + jahr + " ist kein Schaltjahr!");
		}
	}

}
