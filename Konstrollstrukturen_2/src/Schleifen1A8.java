import java.util.Scanner;
public class Schleifen1A8 {

	public static void main(String[] args) {
	Scanner tastatur = new Scanner(System.in);
	
	System.out.print("Wie lang sollen die Seiten des Quadrates sein?");
	int anzahl = tastatur.nextInt();
		
		for (int zeile = 1; zeile <= anzahl; zeile++) {
			for (int spalte = 1; spalte <= anzahl; spalte++) {
				if (zeile == 1 || zeile == anzahl || spalte == 1 || spalte == anzahl) {
					System.out.printf(" * ");
				}
				else {
					System.out.printf("   ");
				}
						
				
			}			
					System.out.println();
				
						
			
		}
	
	}

}
