import java.util.Scanner;
public class FallunterscheidungA4 {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		
		boolean weiter = true;
		
		do {
			
		System.out.print("Geben Sie bitte eine Zahl ein: ");
		int zahl1 = tastatur.nextInt();
		
		System.out.print("Geben Sie bitte noch eine Zahl ein: ");
		int zahl2 = tastatur.nextInt();
		
		System.out.print("Geben Sie bitte einen Operator ein [+], [-], [*], [/]: ");
		char operator = tastatur.next().charAt(0);
		
		int ergebnis = 0;	
				
			
		switch (operator) {		
		
		case '+':
			ergebnis = zahl1 + zahl2;
			System.out.print("Das Ergebnis lautet: " + ergebnis);
			weiter = false;
			break;
			
		case '-':
			ergebnis = zahl1 - zahl2;
			System.out.print("Das Ergebnis lautet: " + ergebnis);
			weiter = false;
			break;
			
		case '*':
			ergebnis = zahl1 * zahl2;
			System.out.print("Das Ergebnis lautet: " + ergebnis);
			weiter = false;
			break;
		
		case '/':
			ergebnis = zahl1 / zahl2;
			System.out.print("Das Ergebnis lautet: " + ergebnis);
			weiter = false;
			break;
		
		default:
			System.out.println("Fehlerhafte Eingabe! Bitte geben Sie einen der Operatoren ein: [+], [-], [*], [/]");
		}
		} while (weiter == true);

	}

}
