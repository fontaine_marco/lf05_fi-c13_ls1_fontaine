import java.util.Scanner;
public class FallunterscheidungA5 {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		
		System.out.print("Geben Sie bitte die Gr��e ein, die berechnet werden soll: [R], [U], [I]");
		char einheit = tastatur.next().charAt(0);
		double ergebnis = 0;
		
		switch (einheit) {		
		
		case 'R':
			System.out.print("Geben Sie bitte den Wert der Spannung [U] ein: ");
			double spannung = tastatur.nextDouble();
			System.out.print("Geben Sie bitte den Wert der Stromst�rke [I] ein: ");
			double strom = tastatur.nextDouble();
			ergebnis = spannung / strom;
			System.out.print("Das Ergebnis lautet " + ergebnis + " Ohm.");
			break;
			
		case 'U':
			System.out.print("Geben Sie bitte den Wert des Widerstandes [R] ein: ");
			double resis = tastatur.nextDouble();
			System.out.print("Geben Sie bitte den Wert der Stromst�rke [I] ein: ");
			double strom2 = tastatur.nextDouble();
			ergebnis = resis * strom2;
			System.out.print("Das Ergebnis lautet " + ergebnis + " V.");
			break;
			
		case 'I':
			System.out.print("Geben Sie bitte den Wert des Widerstandes [R] ein: ");
			double resis2 = tastatur.nextDouble();
			System.out.print("Geben Sie bitte den Wert der Spannung [U] ein: ");
			double spannung2 = tastatur.nextDouble();
			ergebnis = spannung2 / resis2;
			System.out.print("Das Ergebnis lautet " + ergebnis + " A.");
			break;
			
		default:
			System.out.println("Fehlerhafte Eingabe!");
		}

	}
}
