
public class Schleifen1A5 {

	public static void main(String[] args) {
		for (int zeile = 1; zeile < 11; zeile = zeile + 1) {
			for (int spalte = 1; spalte < 11; spalte = spalte + 1) {
				System.out.printf("%6d", zeile * spalte);
			}
			System.out.println();
		}

	}

}
